#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <stdio.h>

using::std::cout;
using::std::string;
using::std::cin;


class Animal
{
	
public:
	
	virtual void Voice()
	{
		cout << "Animals" << "\n";
	}

};

class Dog : public Animal
{
public:

	void Voice() override
	{
		cout << "Woof!" << "\n";
	}
};

class Cat : public Animal
{
public:

	void Voice() override
	{
		cout << "Murr!" << "\n";
	}
};

class Bird : public Animal
{
public:

	void Voice() override
	{
		cout << "Fly!" << "\n";
	}
};

int main()
{
	
	Animal* animals[4] = {new Animal, new Dog, new Cat, new Bird};
	
	for (auto element : animals)
	{
		element->Voice();
	}

}